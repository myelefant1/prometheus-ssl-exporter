import sys
from setuptools import setup

if sys.version_info.major < 3:
    raise RuntimeError('Installing requires Python 3 or newer')
setup(
  name='prometheus-ssl-exporter',
  version='0.3.0',
  description='A SSL Exporter for Prometheus, checking last certificate of the chain.',
  packages=[
    'src',
  ],
  entry_points={
    'console_scripts': [
      'prometheus-ssl-exporter = src.cli:main',
    ],
  },
  install_requires=[
    'prometheus-client',
    'ConfigArgParse',
    'pytest',
    'requests',
    'pyOpenSSL',
    'idna'
  ],
)
  
