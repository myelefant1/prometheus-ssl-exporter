from .scraper import SSLChecker
from urllib.parse import parse_qs, urlparse
import re

known_schemes = {
  'https': 443,
  'ldaps': 636
}

class Handler:

  def request(self, path, query):
    self.path = path
    self.query = parse_qs(query)

  def custom_response(self, status, content):
    return {'type': 'custom', 'status': status, 'content': content}

  def process_response(self, hostname, port):
    return {'type': 'process', 'hostname': hostname, 'port': port}

  def urlise(self, string):
    if not re.match(r'^([a-z]+:)?\/\/', string):
      string = '//' + string
    return string

  def guess_port_number(self, target):
    if target.port:
      return target.port
    if target.scheme not in known_schemes:
      raise Exception
    else:
      return known_schemes[target.scheme]

  def response(self):
    if self.path != '/probe':
      return self.custom_response("200 OK", "SSL Exporter")
    if 'target' in self.query:
      url = self.urlise(self.query['target'][0])
      target = urlparse(url, scheme='https')
    else:
      return self.custom_response("400 Bad Request", "Target parameter is missing")
    if not target.hostname:
      return self.custom_response("400 Bad Request", "Bad target parameter. Should be [<protocol>://]<hostname>")
    self.hostname = target.hostname
    try:
      self.port = self.guess_port_number(target)
    except ValueError:
      return self.custom_response("400 Bad Request", "Non-int port-number")
    except Exception:
      return self.custom_response("400 Bad Request", "Unknown scheme")
    return self.process_response(self.hostname, self.port)
