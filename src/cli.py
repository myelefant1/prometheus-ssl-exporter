from .server import HttpServer
from .request_handler import Handler, known_schemes
from .scraper import SSLChecker
from .metrics_writer import MetricsWriter

from prometheus_client import make_wsgi_app
from wsgiref.util import setup_testing_defaults
from collections import namedtuple

import configargparse

HostInfo = namedtuple(field_names='cert hostname peername', typename='HostInfo')
checker = SSLChecker()
metrics = MetricsWriter()
handler = Handler()
prometheus_app = make_wsgi_app(metrics.registry)

def parse_args():
  p = configargparse.ArgParser(
    prog="prometheus-ssl-exporter"
  )
  p.add(
    '--listen-address',
    default=':9133',
    help='The address to listen on for HTTP requests.',
    type=str,
  )
  options = p.parse_args()
  return options.listen_address.split(':')

def process(hostname, port):
  crypto_cert, peername, hostname = checker.get_certificate(hostname, port)
  hostinfo = HostInfo(cert=crypto_cert, peername=peername, hostname=hostname)
  metrics.write(hostinfo)

def make_response(response):
  return [response.encode("utf-8")]

def guess_port_number(scheme):
  return known_schemes[scheme] if scheme in known_schemes else 443

def app(environ, start_response):
  headers = [('Content-Type', 'text/plain; charset=utf-8')]
  setup_testing_defaults(environ)
  path = environ['PATH_INFO']
  query = environ.get('QUERY_STRING', '')

  handler.request(path, query)
  response = handler.response()

  if response['type'] == 'process':
    try:
      process(response['hostname'], response['port'])
    except TimeoutError as e:
      status = '408 Request Timeout'
      start_response(status, headers)
      return make_response("Request Timeout")
    except Exception as e:
      status = '500 Internal Error'
      start_response(status, headers)
      if hasattr(e, 'sterror'):
        return make_response(e.strerror)
      else:
        return make_response("An error occured")
    return prometheus_app(environ, start_response)
  elif response['type'] == 'custom':
    status = response['status']
    start_response(status, headers)
    return make_response(response['content'])

def main():
  addr, port = parse_args()
  server = HttpServer(addr, int(port))
  server.create(app)
  server.run()
