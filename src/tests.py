import os
import sys
from threading import Thread
import pytest
import requests

from src.cli import app
from src.server import HttpServer
from src.request_handler import Handler, known_schemes

handler = Handler()

#
# Test server listening addr:port
#

def thread_server(server):
  thread = Thread(target=server.single_request)
  try:
    thread.start()
  except Exception as e:
    print("Unable to create thread")
    sys.exit(os.EX_OSERR)

def test_web_server_listening_on_given_address():
  server = HttpServer('127.0.0.1', 9191)
  server.create(app)
  thread_server(server)
  r = requests.get('http://127.0.0.1:9191')
  assert r.status_code == 200

#
# Test Request Handler
#

def test_request_handler_on_get_root_request():
  handler.request('/', '')
  assert handler.response() == {'type': 'custom', 'status': '200 OK', 'content': 'SSL Exporter'}

def test_request_handler_on_non_probe_and_no_params():
  handler.request('/nonprobe', '')
  assert handler.response() == {'type': 'custom', 'status': '200 OK', 'content': 'SSL Exporter'}

def test_request_handler_generic_response_200_on_non_probe():
  handler.request('/other', 'param=nevermind')
  assert handler.response() == {'type': 'custom', 'status': '200 OK', 'content': 'SSL Exporter'}

def test_request_handler_if_no_param_target():
  handler.request('/probe', 'param=notarget')
  assert handler.response() == {'type': 'custom', 'status': '400 Bad Request', 'content': 'Target parameter is missing'}

def test_request_handler_with_scheme_no_port():
  handler.request('/probe', 'target=https://www.example.org')
  assert handler.response() == {'type': 'process', 'hostname': 'www.example.org', 'port': 443}

def test_request_handler_with_scheme_and_port():
  handler.request('/probe', 'target=https://www.example.org:4443')
  assert handler.response() == {'type': 'process', 'hostname': 'www.example.org', 'port': 4443}

def test_request_handler_with_unknown_scheme_with_port():
  handler.request('/probe', 'target=what://www.example.org:55555')
  assert handler.response() == {'type': 'process', 'hostname': 'www.example.org', 'port': 55555}

def test_request_handler_with_unknown_scheme_no_port():
  handler.request('/probe', 'target=what://www.example.org')
  assert handler.response() == {'type': 'custom', 'status': '400 Bad Request', 'content': 'Unknown scheme'}

def test_request_handler_without_scheme_without_port():
  handler.request('/probe', 'target=www.example.org')
  assert handler.response() == {'type': 'process', 'hostname': 'www.example.org', 'port': 443}

def test_request_handler_without_scheme_with_port():
  handler.request('/probe', 'target=www.example.org:12345')
  assert handler.response() == {'type': 'process', 'hostname': 'www.example.org', 'port': 12345}

def test_request_error_on_non_integer_port_parameter():
  handler.request('/probe', 'target=https://www.example.com:nonint')
  assert handler.response() == {'type': 'custom', 'status': '400 Bad Request', 'content': 'Non-int port-number'}

def test_request_handler_with_double_slash_target():
  handler.request('/probe', 'target=//www.example.org')
  assert handler.response() == {'type': 'process', 'hostname': 'www.example.org', 'port': 443}

#
# Test Network
#

def test_request_timeout_on_filtered_host():
  server = HttpServer('127.0.0.1', 9191)
  server.create(app)
  thread_server(server)
  r = requests.get('http://127.0.0.1:9191/probe?target=mozilla.org:9000')
  assert r.status_code == 408
