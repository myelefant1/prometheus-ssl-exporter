# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

- SSL checks are done into different threads

## [0.3.0] - 2020-08-06
### Added
- Default timeout requests to 1.5s
